var express = require('express');
var app = express();

var models = require('./models/index');
/*
app.get('/', function (req, res) {
  res.send('Hello World!');
});*/

app.use(function(req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header('Access-Control-Allow-Methods', 'DELETE, PUT, GET, POST'); 
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
	next();
});

app.use(express.static('public'));

app.use('/api', require('./routes/api'));

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});