'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Stops', // name of Source model
      'streetId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Streets', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Stops', // name of Source model
      'streetId' // key we want to remove
    )
  }
};
