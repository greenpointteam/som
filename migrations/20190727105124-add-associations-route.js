'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'Routes', // name of Source model
      'fromStopId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Stops', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    )
      .then(() => {
        // Payment hasOne Order
        return queryInterface.addColumn(
          'Routes', // name of Target model
          'toStopId', // name of the key we're adding
          {
            type: Sequelize.INTEGER,
            references: {
              model: 'Stops', // name of Source model
              key: 'id',
            },
            onUpdate: 'CASCADE',
            onDelete: 'SET NULL',
          }
        );
      });
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'Routes', // name of Source model
      'fromStopId' // key we want to remove
    )
      .then(() => {
        // remove Payment hasOne Order
        return queryInterface.removeColumn(
          'Routes', // name of the Target model
          'toStopId' // key we want to remove
        );
      });
  }
};
