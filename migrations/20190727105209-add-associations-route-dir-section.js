'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
        'DirectionSections', // name of Source model
        'routeDirectionId', // name of the key we're adding 
        {
          type: Sequelize.INTEGER,
          references: {
            model: 'RouteDirections', // name of Target model
            key: 'id', // key in Target model that we're referencing
          },
          onUpdate: 'CASCADE',
          onDelete: 'SET NULL',
        }
      );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'DirectionSections', // name of Source model
      'routeDirectionId' // key we want to remove
    );
  }
};
