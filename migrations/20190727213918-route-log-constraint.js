'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'RouteLoadLogs', // name of Source model
      'routeId', // name of the key we're adding 
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Routes', // name of Target model
          key: 'id', // key in Target model that we're referencing
        },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn(
      'RouteLoadLogs', // name of Source model
      'routeId' // key we want to remove
    )
  }
};
