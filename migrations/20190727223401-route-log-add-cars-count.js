'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'RouteLoadLogs', // name of Source model
      'unitsCount', // name of the key we're adding 
      {
        type: Sequelize.INTEGER
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('RouteLoadLogs', 'unitsCount');
  }
};
