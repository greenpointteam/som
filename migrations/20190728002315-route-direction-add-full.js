'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn(
      'RouteDirections', // name of Source model
      'fullPath', // name of the key we're adding 
      {
        type: Sequelize.TEXT
      }
    )
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.removeColumn('RouteDirections', 'fullPath');
  }
};
