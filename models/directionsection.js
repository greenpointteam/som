'use strict';
module.exports = (sequelize, DataTypes) => {
  const DirectionSection = sequelize.define('DirectionSection', {
    path: DataTypes.TEXT,
    name: DataTypes.STRING
  }, {
  	getterMethods: {
  		path: function() {
  			return JSON.parse(this.getDataValue('path'));
  		}
  	}
  });
  DirectionSection.associate = function(models) {
    DirectionSection.belongsTo(models.RouteDirection, {as: 'RouteDirection', foreignKey: 'routeDirectionId'});
  };
  return DirectionSection;
};