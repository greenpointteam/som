'use strict';
module.exports = (sequelize, DataTypes) => {
  const Route = sequelize.define('Route', {
    name: DataTypes.STRING,
    type: DataTypes.STRING
  }, {});
  Route.associate = function(models) {
    Route.hasMany(models.RouteDirection, {as: 'directions', foreignKey: 'routeId'});
    Route.hasMany(models.RouteLoadLog, {as: 'logs', foreignKey: 'routeId'});
//    Route.hasOne(models.Stop, {as: 'fromStop', foreignKey: 'fromStopId'});
//    Route.hasOne(models.Stop, {as: 'toStop', foreignKey: 'toStopId'});
  };
  return Route;
};