'use strict';
module.exports = (sequelize, DataTypes) => {
  const RouteDirection = sequelize.define('RouteDirection', {
    type: DataTypes.STRING,
    fullPath: DataTypes.TEXT
  }, {
  	  getterMethods: {
  		fullPath: function() {
  			return JSON.parse(this.getDataValue('fullPath'));
  		}
  	}
  });
  RouteDirection.associate = function(models) {
    RouteDirection.belongsTo(models.Route, {as: 'route', foreignKey: 'routeId'});
    RouteDirection.hasMany(models.DirectionSection, {as: 'sections', foreignKey: 'routeDirectionId'});
  };
  return RouteDirection;
};