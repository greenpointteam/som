'use strict';
module.exports = (sequelize, DataTypes) => {
  const RouteLoadLog = sequelize.define('RouteLoadLog', {
    date: DataTypes.DATE,
    time: DataTypes.TIME,
    load: DataTypes.INTEGER,
    unitsCount: DataTypes.INTEGER
  }, {});
  RouteLoadLog.associate = function(models) {
//    RouteLoadLog.hasOne(models.Route, {as: 'route', foreignKey: 'routeId'})
  };
  return RouteLoadLog;
};