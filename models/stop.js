'use strict';
module.exports = (sequelize, DataTypes) => {
  const Stop = sequelize.define('Stop', {
    name: DataTypes.STRING,
    centerLeft: DataTypes.STRING,
    centerRight: DataTypes.STRING,
    irkbusId: DataTypes.INTEGER
  }, {});
  Stop.associate = function(models) {
    Stop.hasOne(models.Street, {as: 'street', foreignKey: 'streetId'})
  };
  return Stop;
};