'use strict';
module.exports = (sequelize, DataTypes) => {
  const StopLoadLog = sequelize.define('StopLoadLog', {
    date: DataTypes.DATE,
    time: DataTypes.TIME,
    load: DataTypes.INTEGER
  }, {});
  StopLoadLog.associate = function(models) {
    // associations can be defined here
  };
  return StopLoadLog;
};