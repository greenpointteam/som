'use strict';
module.exports = (sequelize, DataTypes) => {
  const Street = sequelize.define('Street', {
    name: DataTypes.STRING
  }, {});
  Street.associate = function(models) {
    Street.hasMany(models.Stop, {as: 'stops', foreignKey: 'streetId'});
  };
  return Street;
};