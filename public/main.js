
ymaps.ready(init);
$('#color_pline').simpleColor();
var polyline;
var polyline1;
var polyline2;
var polyline3;
var geoObjects;

function init() {

    function randColor() {
    var r = Math.floor(Math.random() * (256)),
        g = Math.floor(Math.random() * (256)),
        b = Math.floor(Math.random() * (256));
        return '#' + r.toString(16) + g.toString(16) + b.toString(16);
    }

    var myMap = new ymaps.Map('map', {
        center: [52.276778071887306, 104.28613099999997],
        zoom: 14,
        behaviors: ['drag', 'rightMouseButtonMagnifier']
    });

    var routePanelControl = new ymaps.control.RoutePanel({
        options: {
            maxWidth: '180px', 
            float: 'right'
        }
    });
    myMap.controls.add(routePanelControl);

    myMap.controls.add('routeEditor');

    // получаем маршрут

    var makePolyline = function (data) {
        var x = data;
            // Создаем ломаную, используя класс GeoObject.
    var myGeoObject = new ymaps.GeoObject({
            // Описываем геометрию геообъекта.
            geometry: {
                // Тип геометрии - "Ломаная линия".
                type: "LineString",
                // Указываем координаты вершин ломаной.
                coordinates: x
            },
            // Описываем свойства геообъекта.
            properties:{
                // Содержимое хинта.
                hintContent: "Я геообъект",
                // Содержимое балуна.
                balloonContent: "Меня можно перетащить"
            }
        }, {
            // Задаем опции геообъекта.
            // Включаем возможность перетаскивания ломаной.
            draggable: true,
            // Цвет линии.
            strokeColor: randColor(),
            // Ширина линии.
            strokeWidth: 4
        });

        myGeoObject.editor.startEditing(); 
        myGeoObject.editor.startDrawing();

        return myMap.geoObjects.add(myGeoObject);
    };

     

    //получение данных от сервера

    var processData = function (i) {
            var d = i.directions[0].fullPath;
            var dAr = d.map(function(p) {
              return [p.lt, p.lg];
           });
           return dAr;
    }
    var dataNew = fetch('http://192.168.10.152:3000/api/routes');

            dataNew

                .then(response => response.json()) // преобразуем ответ в json

                .then(data => {
                    data.forEach(route => makePolyline(processData(route)));
                })

                .catch(error => console.error(error));
    // завершение получения данных от сервера

    var myPieChart = new ymaps.Placemark([52.2796778071887406,104.29613099999997], {
            // Данные для построения диаграммы.
            data: [
                {weight: 8, color: '#0E4779'},
                {weight: 6, color: '#1E98FF'}
            ],
            iconCaption: "Диаграмма",
            iconContent: ''
        }, 
        {
            // Зададим произвольный макет метки.
            iconLayout: 'default#pieChart',
            // Радиус диаграммы в пикселях.
            iconPieChartRadius: 30,
            // Радиус центральной части макета.
            iconPieChartCoreRadius: 10,
            // Стиль заливки центральной части.
            iconPieChartCoreFillStyle: '#ffffff',
            // Cтиль линий-разделителей секторов и внешней обводки диаграммы.
            iconPieChartStrokeStyle: '#ffffff',
            // Ширина линий-разделителей секторов и внешней обводки диаграммы.
            iconPieChartStrokeWidth: 3,
            // Максимальная ширина подписи метки.
            iconPieChartCaptionMaxWidth: 200
        });

        polyline1 = new ymaps.Polyline([
                    [52.276778071887306, 104.28613099999997],
                    [52.285 + 0.0002, 104.288],
                    [52.287 + 0.0002, 104.300],
                    [52.289, 104.302]
                ], {
                    //hintContent: "Ломаная"
        }, {
            draggable: true,
            strokeColor: randColor(),
            strokeWidth: 4
            // Первой цифрой задаем длину штриха.
            // Второй — длину разрыва.
            //strokeStyle: '1 5'
        });
                myMap.geoObjects.add(polyline1);
                polyline1.editor.startEditing(); 
                polyline1.editor.startDrawing(); 

                polyline2 = new ymaps.Polyline([
                    [52.276778071887306, 104.28613099999997],
                    [52.285 + 0.0001, 104.288], /* (Math.random() / 1000) */
                    [52.287 + 0.0001, 104.300],
                    [52.289, 104.302]
                ], {
                    //hintContent: "Ломаная"
        }, {
            draggable: true,
            strokeColor: randColor(),
            strokeWidth: 4
            // Первой цифрой задаем длину штриха.
            // Второй — длину разрыва.
            //strokeStyle: '1 5'
        });
                myMap.geoObjects.add(polyline2);
                polyline2.editor.startEditing(); 
                polyline2.editor.startDrawing(); 

                polyline3 = new ymaps.Polyline([
                    [52.276778071887306, 104.28613099999997],
                    [52.285, 104.288],
                    [52.287, 104.300],
                    [52.289, 104.302]
                ], {
                    //hintContent: "Ломаная"
        }, {
            draggable: true,
            strokeColor: randColor(),
            strokeWidth: 4
            // Первой цифрой задаем длину штриха.
            // Второй — длину разрыва.
            //strokeStyle: '1 5'
        });
                myMap.geoObjects.add(polyline3);
                polyline3.editor.startEditing(); 
                polyline3.editor.startDrawing(); 

    $('#showDiagram').click(
        function () {
            myMap.geoObjects.add(myPieChart);
        }); 

    $('#hideDiagram').click(
        function () {
            myMap.geoObjects.remove(myPieChart);
        });            

    $('#addPolyline').click(
                function () {
 
                $('#stopEditPolyline').attr('disabled', false);
 
                var widthpl = $('#width_line').attr('value');
                var colorpl = $('#color_pline').attr('value');
                var transparencypl = $('#transparency_pline').attr('value');
 
                polyline = new ymaps.Polyline([], {}, {
                    draggable: true,
                    strokeColor: colorpl,
                    strokeOpacity: transparencypl,
                    strokeWidth: widthpl
                    //strokeStyle: [1, 5]
                });
 
                myMap.geoObjects.add(polyline);
                polyline.editor.startEditing(); 
                polyline.editor.startDrawing(); 
 
                $('#addPolyline').attr('disabled', true);           
            });             
 
            // Обработка нажатия на кнопку Завершить редактирование 
            $('#stopEditPolyline').click(
                function () {   
 
                $('#stopEditPolyline').attr('disabled', true);
                $('#addPolyline').attr('disabled', false); 
                if (!polyline) return;
                polyline.editor.stopEditing();
                printGeometry(polyline.geometry.getCoordinates());
            }); 
 
 
            // Обработка нажатия на кнопку Удалить
            $('#dellPolyline').click(
                function () {   
 
                myMap.geoObjects.remove(polyline);
                $('#geometry').html('');
                $('#addPolyline').attr('disabled', false);
            });
 
        }

        
// Выводит массив координат геообъекта в <div id="geometry">
        function printGeometry (coords) {
            $('#geometry').html('Координаты: ' + stringify(coords));
 
            function stringify (coords) {
                var res = '';
                if ($.isArray(coords)) {
                    res = '[ ';
                    for (var i = 0, l = coords.length; i < l; i++) {
                        if (i > 0) {
                            res += ', ';
                        }
                        res += stringify(coords[i]);
                    }
                    res += ' ]';
                } else if (typeof coords == 'number') {
                    res = coords.toPrecision(6);
                } else if (coords.toString) {
                    res = coords.toString();
                }
 
                return res;
            }

            
        }
