const _ = require('lodash');
const express = require('express');
const router = express.Router();
const models = require('../models/index');

router.get('/routes/:routeName', function(req, res, next) {
	const name = _.get(req, ['params', 'routeName']);

	if (!name) return next('Route id required');

	models.Route.findAll({
        include: [
//            {model: models.User, as: 'user'},
            {
            	model: models.RouteDirection,
            	as: 'directions',
            	include: [
            		{
            			model: models.DirectionSection,
            			as: 'sections'
            		}
            	]
            }
        ],
        where: { name }
    })
		.then(routes => {
			if (!routes.length) throw 'Route not found'; 
			res.json(routes[0]);
		}).catch(next);
});

router.post('/routes/:routeName/path', function(req, res, next) {
	
});

router.get('/routes', function(req, res, next) {

	models.Route.findAll({
        include: [
//            {model: models.User, as: 'user'},
            {
            	model: models.RouteDirection,
            	as: 'directions',
            	include: [
            		{
            			model: models.DirectionSection,
            			as: 'sections'
            		}
            	]
            }
        ]
    })
		.then(routes => {
			res.json(routes);
		}).catch(next);
});

router.get('/logs/routes/:routeName/date/:logDate', function(req, res, next) {
	const { routeName, logDate } = req.params;

	res.end();
	console.log(routeName, logDate);
});

module.exports = router;
