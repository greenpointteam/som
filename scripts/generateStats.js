const models = require('../models/index');
const Sequelize = require('sequelize');
const moment = require('moment');

const dateString = '2019-07-25';
const initHour = 7;
const timedt = 2;

const routeNum = 1;

const logs = [];

let carCount = 10

for (let i = 0; i < 7; i++) {
	let tm = genTime(initHour + i * timedt);

	let load = getRandomInt(50, 100);

	if (i < 1 || i > 5) load-=15;

	if (i > 2 && i < 4) load+=30;

	logs.push({
		date: dateString,
		time: tm,
		load,
		routeId: routeNum,
		unitsCount: i < 3 ? (carCount - 3) : carCount
	});
}

//console.log(logs);

models.RouteLoadLog.bulkCreate(logs).then(console.log, console.error);

function getRandomInt(min, max) {
 	return Math.floor(Math.random() * (max - min)) + min;
}

function genTime(hour) {
	const time = 60 * 60 * hour;

	const timeMom = moment.duration(time * 1000);
	let th = timeMom.hours();
	let tm = timeMom.minutes();
	let ts = timeMom.seconds();

	th = th < 10 ? `0${th}` : th;
	tm = tm < 10 ? `0${tm}` : tm;
	ts = ts < 10 ? `0${ts}` : ts;

	return `${th}:${tm}:${ts}`;
}