const request = require('request');
var models = require('../models/index');

request('http://irkbus.ru/php/getStations.php?city=irkutsk', (error, response, body) => {

	const stops = JSON.parse(body);

	const names = [];
	const queryStops = [];

	stops.forEach(row => {
		if (names.indexOf(row.name) !== - 1) return;

		const lt = row.lat / 1000000;
		const lg = row.lng / 1000000;

		const coo = JSON.stringify({ lt, lg });

		queryStops.push({
			centerLeft: coo,
			centerRight: coo,
			name    : row.name,
			irkbusId: row.id
		});

		names.push(row.name);
	})

	models.Stop.bulkCreate(queryStops).then(console.log, console.log);

});
